sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageBox'
], function(Controller, MessageBox) {
	"use strict";
	
	return Controller.extend("nnext.iq.View.controller.View1", {
		onInit:function(){
			var oModel = new sap.ui.model.xml.XMLModel();
			oModel.loadData("Data/SignData.xml");
			//core 
			//sap.ui.getCore().setModel(oModel);
			//view
			this.getView().setModel(oModel);
		},
		acceptInfoBox: function(oEvent) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.information(
				"您點選了接受", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},
		rejectInfoBox: function(oEvent) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.information(
				"您點選了拒絕", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},
		confirmMsgBox: function(oEvent) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.confirm(
				"確定修改這筆資料?", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		}
	});
});